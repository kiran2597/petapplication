package com.hcl.pp.service;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.pp.customException.UserException;
import com.hcl.pp.dao.UserDao;
import com.hcl.pp.dao.UserDaoImpl;
import com.hcl.pp.model.User;

@Service
public class SecurityServiceImpl implements SecurityService{

	
	private static final Logger LOGGER = Logger.getLogger(SecurityServiceImpl.class);
	
	@Autowired
	private UserDao userDao;
	@Override
	public boolean authenticateUser(User userDetails) throws UserException {
		
		boolean check = false;
		if(userDetails!=null) {
			check = userDao.authenticateUser(userDetails);
		}
		return check;
	}

}
