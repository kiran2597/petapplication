package com.hcl.pp.service;

import java.util.List;

import com.hcl.pp.customException.UserException;
import com.hcl.pp.model.Pet;

public interface PetService {

	public void savePet(Pet pet);
	public List<Pet> getAllPets() throws UserException;
	public Pet getPetById(Long id) throws UserException;
	
}
