package com.hcl.pp.service;

import java.util.List;
import java.util.Set;

import com.hcl.pp.customException.UserException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;

public interface UserService {

	public boolean addUser(User user);
	public boolean updateUser(User user);
	public List<User> listUsers();
	public User getUserById(Long id);
	public User findByUserName(String username);
	public boolean removeUser(User user);
	public boolean buyPet(Pet pet,Long userId) throws UserException;
	public Set<Pet> getMyPets(String name);
}
