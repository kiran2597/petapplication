package com.hcl.pp.service;

import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.apache.log4j.lf5.util.LogFileParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.pp.customException.UserException;
import com.hcl.pp.dao.UserDao;
import com.hcl.pp.dao.UserDaoImpl;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.mysql.cj.log.Log;

@Service("userServiceImpl")
public class UserServiceImpl implements UserService{

	
	private static final Logger LOGGER = Logger.getLogger(UserServiceImpl.class);
	
	@Autowired
	private UserDao userDao;
	
	@Transactional
	@Override
	public boolean addUser(User user) {
		
		boolean check=false;
		try {
			check = userDao.addUser(user);
		}catch(Exception e){
			LOGGER.error(e.getMessage());
		}
		return check;
	}

	@Override
	@Transactional
	public boolean updateUser(User user) {
		
		boolean check=false;
		try {
			check = userDao.updateUser(user);
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
		}
		
		return check;
	}

	@Override
	@Transactional
	public List<User> listUsers() {
		
		List<User> users=null;
		try {
			users= userDao.listUsers();
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
		}
		return users;
		
	}

	@Override
	@Transactional
	public User getUserById(Long id) {
		
		User user = null;
		try {
			user= userDao.getUserById(id);
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
		}
		return user;
	}

	@Override
	@Transactional
	public User findByUserName(String username) {
		
		User user = null;
		try {
			user= userDao.findByUserName(username);
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
		}
		return user;
		
	}

	@Override
	@Transactional
	public boolean removeUser(User user) {
		
		boolean check = false;
		try {
			check = userDao.removeUser(user);
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
		}
		return check;
	}

	@Override
	public boolean buyPet(Pet pet, Long userId) throws UserException {
		
		boolean check = false;
		try {
			check = userDao.buyPet(pet, userId);
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
		}
		return check;
	}

	@Override
	public Set<Pet> getMyPets(String name) {
		
		Set<Pet> pets = null;
		try {
			pets = userDao.getMyPets(name);
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
		}
		return pets;
	}

}
