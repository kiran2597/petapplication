<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<link  rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/login.css" >
<style type="text/css">
	.form-box {
	width: 400px;
	height: 400px;
	position: relative;
	margin: 9% auto;
	background: #fff;
	padding: 5px;
	overflow: hidden;
}
body {
	background-color: #2c567a;
}

h2 {
	margin: 35px auto;
}

input[type=text], [type=password] {
	width: 340px;
	padding: 12px 20px;
	margin: 10px 0;
	display: inline-block;
	border: 1px solid #ccc;
	border-radius: 4px;
}
#userError,#passError{
	font-size:15px;
	text-align:center;
	color:red;
}
input[type=submit] {
	width: 340px;
	padding: 12px 20px;
	margin: 10px 0;
	display: inline-block;
	border: 1px solid #ccc;
	border-radius: 4px;
}
p {
	font-family: arial;
}
</style>
<script type="text/javascript">
function checkusername(){
	var username = document.login.username.value;
	if(username=="" || username==null){
		document.getElementById("userError").innerHTML = "Username should not be empty";
		document.login.username.focus();
		return false;
	}
}
function checkpassword(){
	var password = document.login.password.value;
	if(password=="" || password==null){
		document.getElementById("passError").innerHTML = "Password should not be empty";
		document.login.password.focus();
		return false;
	}
}

</script>
</head>
<body>
	<form:form action="./authenticate" name="login" modelAttribute="userData" method="post">
		<div class="form-box" align="center">
			<h2 align="center">Login</h2>
			${message}
			<div class="input">
				<table>
					<tr>
						<form:input type="hidden"  path="id"/>
						<td><label>Username</label>
						<form:input type="text" class="form-control" path="username" id="username"
							placeholder="Enter username" onblur="checkusername()" /><br>
						<span id="userError"></span>
						</td>
					</tr>
					<tr>
						<td><label>Password</label>
						<form:input type="password" class="form-control" id="password"
							path="userPassword" placeholder="enter password" onblur="checkpassword()" /><br>
						<span  id="passError"></span>
						</td>
					</tr>
				</table>
				<input type="submit" class="btn btn-success" value="Login">
				<p>
					Not a member?&nbsp;<a href="./user/add">SignUp</a>
				</p>
			</div>
		</div>
	</form:form>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>

</html>